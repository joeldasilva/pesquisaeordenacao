/*
 * Implementações  para a Disciplina de Pesquisa e Ordenação de Dados
 * Universidade Federal de Santa Maria
 */
package pod;
import java.util.Random;

/**
 *
 * @author Joel da Silva
 */
public class OrdenaInteirosApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println("=================================");
        //define a quantidade de elementos do vetor
        int tamanho = 10;  
        
        
        //cria o vetor 
        int[] v = new int[tamanho];
       
        //insere valores aleatórios no vetor criado
        insereValores(v);
        
        //imprime valores;
         System.out.println("===== Vetor Original =====");
        imprimeValores(v);
        
        //ordena o vetor com insertion sort
        int n = v.length;
        long tempoInicial = System.currentTimeMillis();
       // InsertionSort(v,n);
        SelectionSort(v,n);
      // BubbleSort(v,n);
       // ShellSort(v,n);
   
       
       long tempoFinal = System.currentTimeMillis();
     // System.out.printf("Tempo de Ordenação %.3f ms%n", (tempoFinal - tempoInicial) / 1000d);  
        
        //imprime valores ordenados;
        System.out.println("===== Vetor Ordenado =====");
        imprimeValores(v);
        
        System.out.println("=================================");
        
     
    }
    
    //recebe um vetor e preenche com valores aleatórios
    static void insereValores(int[] v){
         // instancia um objeto da classe Random 
       // Random gerador = new Random(1000); //gera sempre os mesmos valores
         Random gerador = new Random(); //gera valores diferentes a cada vez
         int n = v.length;
  
        //insere valores randômicos no vetor (valores entre 0 e 99)
        for (int i = 0; i < n; i++){
          v[i] = gerador.nextInt(100);       
        }
    }
    
    
    //recebe um vetor e imprime os elementos
    static void imprimeValores(int[] v) {
        int n = v.length;
         //imprime valores do vetor
        for (int i = 0; i < n; i++){
           // System.out.println(elementos[i]);
           System.out.printf("[%d] = %2d\n", i, v[i]);
        }
    }
    
    
  /*
   * =============== ALGORITMOS DE ORDENAÇÃO ===============
   */
    
   
    
/*
    Ordena um vetor de comprimento n com uma implementação do Insertion Sort
    @param v vetor a ser ordenado
    @param n tamanho do vetor
*/
    static void InsertionSort(int[] v, int n)
    {      
      //  int comparacoes = 0;
      //  int realocacoes =0;
        for (int i=1; i<n; ++i)
            
        {
            int key = v[i];
            int j = i-1;    
             //   Teste de Mesa - Debug
            // System.out.printf("==== Passo(%d) i=%d, j=%d, key=%d ==== \n",i,i,j,key);
           //  System.out.println("Situação Inicial");
         //   imprimeValores(v);
             
            while (j>=0 && v[j] > key)
            {             
                v[j + 1] = v[j];                
                j = j-1;
             //   realocacoes ++;
            }    
            v[j+1] = key;   
         //   System.out.println("Situação Final");
          //  comparacoes++;
          // Teste de Mesa - Debug
          //  System.out.printf("==== Comparações=%d, Realocações=%d ==== \n",comparacoes,realocacoes);
         //   realocacoes =0;
        //    imprimeValores(v);
           
        }
    
    }  
    
    
    /*
    Ordena um vetor de comprimento n com uma implementação do Selection Sort
    @param v vetor a ser ordenado
    @param n tamanho do vetor
*/
    
 static void SelectionSort(int[] v, int n)
    {
        int i,j,menor_idx,aux;
        for (i = 0; i < n-1; i++)
            
        {
            // Encontra o índice do menor elemento do vetor
            menor_idx = i;
       
              for (j = i+1; j < n; j++)
                if (v[j] < v[menor_idx]) {
                    menor_idx = j;
                }
            System.out.printf("==== Passo(%d) i=%d ==== \n",i,i);
            imprimeValores(v);
            // efetua a troca 
            aux = v[menor_idx];
            v[menor_idx] = v[i];
            v[i] = aux;
           
        } 
    }
    
    
    /*
    Ordena um vetor de comprimento n com uma implementação do Bubble Sort
    @param v vetor a ser ordenado
    @param n tamanho do vetor
*/
    
 static void BubbleSort(int[] v, int n)
    {
        int i, j,aux;
        for(i=n-1; i>0; i--){ 
         for(j=0; j<i; j++){  
             System.out.printf("==== Passo(%d) i=%d, j=%d ==== \n",i,i,j);
             imprimeValores(v);
            if( v[j]> v[j+1]){
                aux = v[j];
                v[j] = v[j+1];
                v[j+1] = aux;
            }
         }
      }
    }
 
 
 /*
    Ordena um vetor de comprimento n com uma implementação do Sheel Sort
    @param v vetor a ser ordenado
    @param n tamanho do vetor
*/
    
 static void ShellSort(int[] v, int n)
    {
        int h = 1;
        while(h < n) {
                h = h * 3 + 1;
        }

        h = h / 3;
        int c, j;

        while (h > 0) {
            for (int i = h; i < n; i++) {
                c = v[i];
                j = i;
                while (j >= h && v[j - h] > c) {
                    v[j] = v[j - h];
                    j = j - h;
                }
                v[j] = c;
            }
            h = h / 2;
        }
 
    }
  
   

} //fim principal
